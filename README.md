# README #

HD-189768. Se solicita la creación de un sistema de alertas por email a Hotline cuando se encolen mensajes en la integración Vitropath a Vantage en H. Costa del Sol. Se adjunta canales de TEST para poder comprobar la funcionalidad. Se envían mensajes cada 2 horas para no desbordar el buzón de HL.

### What is this repository for? ###

* Se añaden canales de test que permiten encolar rápidamente mensajes, para poder realizar pruebas en local
* Version Mirth 3.3.0.7801

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests - desplegar canales de test y detener el TEST_03_receive_OML-O21 para que se encole la mensajería en el TEST_02_queue_EF_OML-O21
* Deployment instructions - desplegar en orden inverso: 03, 02, 01

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact